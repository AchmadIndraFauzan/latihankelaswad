@extends('layouts.app')

@section('content')
  <div class="container">
    <h3 align="center">Edit Notes</h3>
    <form action="{{ route('notes.update', ['note' => $note->id]) }}" method="post">
      {{ csrf_field() }}
      @method('PUT')
      <div class="form-group">
        <label for="inputTitle">Title</label>
        <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Enter title" value="{{$note->title}}">
      </div>
      <div class="form-group">
        <label for="inputNote">Note</label>
        <input type="text" class="form-control" id="inputNote" name="note" placeholder="Enter note" value="{{$note->note}}">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                  Dashboard
                  <a href="{{ route('notes.create') }}" style="float: right">Create</a>
                </div>

                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Note</th>
                        <th scope="col">Date Created</th>
                        <th scope="col" colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($note as $key=>$posts)
                        <tr>
                          <td>{{ ++$key }}</td>
                          <td>{{ $posts->title }}</td>
                          <td>{{ $posts->note }}</td>
                          <td>{{ $posts->created_at }}</td>
                          <td>
                            <a href="{{ route('notes.edit', ['note' => $posts->id]) }}">
                              <button type="button" class="btn btn-primary">Edit</button>
                            </a>
                          </td>
                          <td>
                            <form action="{{ route('notes.destroy', ['note' => $posts->id]) }}" method="post">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

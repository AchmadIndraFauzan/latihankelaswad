@extends('layouts.app')

@section('content')
  <div class="container">
    <h3 align="center">Add Notes</h3>
    <form action="{{ route('notes.store') }}" method="post">
      {{ csrf_field() }}
      <div class="form-group">
        <label for="inputTitle">Title</label>
        <input type="text" class="form-control" id="inputTitle" name="title" placeholder="Enter title">
      </div>
      <div class="form-group">
        <label for="inputNote">Note</label>
        <input type="text" class="form-control" id="inputNote" name="note" placeholder="Enter note">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
@endsection

<?php

use Illuminate\Database\Seeder;
use App\Notes;
class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $note = new Notes;
      $note->title = Str::random(5);
      $note->note = Str::random(20);
      $note->save();
    }
}
